
//
// Credit to fmalina python
// https://github.com/fmalina/bng_latlon/blob/master/latlon_to_bng.py


// py = from math import sqrt, pi, sin, cos, tan, atan2
import 'dart:math';

void WGS84toOSGB36(lat, lon) {
  /*
  """ Accept latitude and longitude as used in GPS.
  Return OSGB grid coordinates: eastings and northings.
  Usage:
  >>> from latlon_to_bng import WGS84toOSGB36
  >>> WGS84toOSGB36(51.4778, -0.0014)
  (538890.1053365842, 177320.49650700082)
  >>> WGS84toOSGB36(53.50713, -2.71766)
  (352500.19520169357, 401400.01483428996)
  """
  */

  // First convert to radians
  // These are on the wrong ellipsoid currently: GRS80. (Denoted by _1)
  /*
    lat_1 = lat*pi/180
    lon_1 = lon*pi/180
   */
  var lat_1 = lat * pi / 180;
  var lon_1 = lon * pi / 180;

  // Want to convert to the Airy 1830 ellipsoid, which has the following:
  // The GSR80 semi-major and semi-minor axes used for WGS84(m)
  /*
    a_1, b_1 = 6378137.000, 6356752.3141
     */
  const a_1 = 6378137.000;
  const b_1 = 6356752.3141;

  // The eccentricity of the GRS80 ellipsoid
  /* e2_1 = 1 - (b_1*b_1)/(a_1*a_1)  # The eccentricity of the GRS80 ellipsoid
   */
  var e2_1 = 1 - (b_1 * b_1) / (a_1 * a_1);


  /* nu_1 = a_1 / sqrt(1-e2_1 * sin(lat_1) **2) */
  var nu_1 = a_1 / sqrt(1-e2_1 * sin(lat_1) **2);

  // First convert to cartesian from spherical polar coordinates
  /* py
  # First convert to cartesian from spherical polar coordinates
  H = 0  # Third spherical coord.
  x_1 = (nu_1 + H)*cos(lat_1)*cos(lon_1)
  y_1 = (nu_1 + H)*cos(lat_1)*sin(lon_1)
  z_1 = ((1-e2_1)*nu_1 + H)*sin(lat_1)
  */

  //===  Third spherical coord.
  var H = 0;
  var x_1 = (nu_1 + H) * cos(lat_1) * cos(lon_1);
  var y_1 = (nu_1 + H) * cos(lat_1) * sin(lon_1);
  var z_1 = ((1-e2_1) * nu_1 + H) * sin(lat_1);




  /*
  # Perform Helmut transform (to go between GRS80 (_1) and Airy 1830 (_2))

  s = 20.4894*10**-6  # The scale factor -1
  # The translations along x,y,z axes respectively
  tx, ty, tz = -446.448, 125.157, -542.060
  # The rotations along x,y,z respectively, in seconds
  rxs, rys, rzs = -0.1502, -0.2470, -0.8421
  # In radians
  rx, ry, rz = rxs*pi/(180*3600.), rys*pi/(180*3600.), rzs*pi/(180*3600.)
  x_2 = tx + (1+s)*x_1 + (-rz)*y_1 + (ry)*z_1
  y_2 = ty + (rz)*x_1 + (1+s)*y_1 + (-rx)*z_1
  z_2 = tz + (-ry)*x_1 + (rx)*y_1 + (1+s)*z_1
 */
  //=== Perform Helmut transform (to go between GRS80 (_1) and Airy 1830 (_2))

  //The scale factor -1
  var s = 20.4894 * 10 ** -6;
  // The translations along x,y,z axes respectively
  //tx, ty, tz = -446.448, 125.157, -542.060
  var tx = -446.448;
  var ty =  125.157;
  var tz = -542.060;

  // The rotations along x,y,z respectively, in seconds
  // rxs, rys, rzs = -0.1502, -0.2470, -0.8421
  var rxs = -0.1502;
  var rys = -0.2470;
  var rzs = -0.8421;

  // In radians
  // rx, ry, rz = rxs*pi/(180*3600.), rys*pi/(180*3600.), rzs*pi/(180*3600.)
  var rx = rxs * pi/(180 * 3600);
  var ry = rys * pi/(180 * 3600);
  var rz = rzs * pi/(180 * 3600);

  /*
  x_2 = tx + (1+s)*x_1 + (-rz)*y_1 + (ry)*z_1
  y_2 = ty + (rz)*x_1 + (1+s)*y_1 + (-rx)*z_1
  z_2 = tz + (-ry)*x_1 + (rx)*y_1 + (1+s)*z_1
   */
  var x_2 = tx + (1+s) *x_1 + (-rz) *y_1 + (ry) * z_1;
  var y_2 = ty + (rz)  *x_1 + (1+s )*y_1 + (-rx) * z_1;
  var z_2 = tz + (-ry) *x_1 + (rx) *y_1 + (1+s) * z_1;


}
